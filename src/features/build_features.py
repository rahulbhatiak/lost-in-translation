import functools
import os

import click
import pandas as pd
import stanza
import google
from google.cloud import translate_v2 as translate
from joblib import Memory
from tqdm.auto import tqdm
from nltk.sentiment import SentimentIntensityAnalyzer 


@functools.cache
def get_translate_client():
    translate_client = translate.Client()
    return translate_client


def inner_translate(word, counter):
    translate_client = get_translate_client()
    result = translate_client.translate(word)
    counter += 1
    return result.get('translatedText', "NOTRANS")


def translate_column(input_filepath, output_filepath, column):
    """Translate the column"""
    tqdm.pandas()
    full_memory = Memory(location=os.path.join(output_filepath, ".full_translation_cache"), verbose=0)
    output_file = os.path.join(output_filepath, f"{column}_full_translation.csv")
    if os.path.exists(output_file):
        print(f"Translation already exists for {column}")
        return output_file 
    cache_translate = full_memory.cache(inner_translate)
    chequeado_df = pd.read_csv(os.path.join(input_filepath, "chequeado_spanish_processed.csv"))
    counter = 0
    assert os.environ.get("GOOGLE_APPLICATION_CREDENTIALS") is not None, "Need to set GOOGLE_APPLICATION_CREDENTIALS"
    chequeado_df.loc[:, f"{column}_full_translation"] = chequeado_df[column].progress_apply(lambda x: cache_translate(x, counter))
    print(f"Called api {counter} times")
    chequeado_df[[f"{column}_full_translation"]].to_csv(output_file)
    return output_file


def compute_sentiment(translated_file, output_filepath, column):
    if not os.path.exists(translated_file):
        raise ValueError(f"Couldn't find {translated_file}")
    translated_file = pd.read_csv(translated_file, index_col=0)
    output_file = os.path.join(output_filepath, f"{column}_en_sentiment.csv")
    if os.path.exists(output_file):
        print(f"Sentiment already exists for {column}")
        return output_file
    sia = SentimentIntensityAnalyzer()
    trans_col = translated_file.columns[0]
    sentiment = (
        translated_file
        .progress_applymap(lambda x: sia.polarity_scores(x)['compound'])
        .rename(
            columns={
            trans_col: f"{column}_en_sentiment",
        }))
    sentiment.to_csv(output_file)
    return output_file


def compute_lemmatized_sentiment(translated_file, output_filepath, column):
    if not os.path.exists(translated_file):
        raise ValueError(f"Couldn't find {translated_file}")
    translated_file = pd.read_csv(translated_file, index_col=0)
    output_file = os.path.join(output_filepath, f"{column}_en_lemma_sentiment.csv")
    if os.path.exists(output_file):
        print(f"Sentiment already exists for {column}")
        return output_file
    trans_col = translated_file.columns[0]
    sia = SentimentIntensityAnalyzer()
    nlp_en = stanza.Pipeline(lang='en', processors='tokenize,lemma')
    def lemmatize(x):
        return ".".join(
        [
            " ".join([word.lemma for word in sentence.words])
            for sentence in nlp_en(x).sentences
        ]
    )
    translated_file[f'lemmatized_{trans_col}'] = translated_file[trans_col].progress_apply(lemmatize)
    lemma_sent = (
        translated_file[[f'lemmatized_{trans_col}']]
        .progress_applymap(lambda x: sia.polarity_scores(x)['compound'])
        .rename(columns={f'lemmatized_{trans_col}': f"{column}_en_lemma_sentiment"})
    )
    lemma_sent.to_csv(output_file)
    return output_file



@click.command("translate-column")
@click.argument("input-filepath")
@click.argument("output-filepath")
@click.argument("column")
def main(input_filepath, output_filepath, column):
    column = column.replace(" ", "_")
    translated_file = translate_column(input_filepath, output_filepath, column)
    print(f"Translated File: {translated_file}")
    sentiment_file = compute_sentiment(translated_file, output_filepath, column)
    print(f"Sentiment File: {sentiment_file}")
    lemmatized_sentiment_file = compute_lemmatized_sentiment(translated_file, output_filepath, column)
    print(f"Lemmatized Sentiment File: {lemmatized_sentiment_file}")


if __name__ == "__main__":
    main()