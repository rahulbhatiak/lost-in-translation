import click
import matplotlib.pyplot as plt
import os
import pandas as pd
import seaborn as sns

TITLE_STRING = "Título"
DESCRIPTION_STRING = "Descripción"

TITLES = {
   f"{TITLE_STRING}_en_lemma_sentiment": f"{TITLE_STRING} EN Lemmatized Sentiment",
   f"{TITLE_STRING}_en_sentiment": f"{TITLE_STRING} EN Raw Sentiment",
   f"{DESCRIPTION_STRING}_en_lemma_sentiment": f"{DESCRIPTION_STRING} EN Lemmatized Sentiment",
   f"{DESCRIPTION_STRING}_en_sentiment": f"{DESCRIPTION_STRING} EN Raw Sentiment",
}


@click.command('visualize')
@click.argument("input_path")
@click.argument("output_path")
def visualize(input_path, output_path):
    output_dir = os.path.join(output_path, "descriptive")
    os.makedirs(output_dir, exist_ok=True)
    en_title_lemma_sent_file = os.path.join(input_path, f"{TITLE_STRING}_en_lemma_sentiment.csv")
    en_title_sent_file = os.path.join(input_path, f"{TITLE_STRING}_en_sentiment.csv")
    en_desc_lemma_sent_file = os.path.join(input_path, f"{DESCRIPTION_STRING}_en_lemma_sentiment.csv")
    en_desc_sent_file = os.path.join(input_path, f"{DESCRIPTION_STRING}_en_sentiment.csv")
    sns.set()
    sns.set_context('talk')
    for column in [en_title_sent_file, en_title_lemma_sent_file, en_desc_lemma_sent_file, en_desc_sent_file]:
        df = pd.read_csv(column, index_col=0)
        col = df.columns[0]
        fig, ax = plt.subplots()
        sns.histplot(df, kde=True, legend=False, ax=ax)
        ax.set_title(TITLES[col])
        fig.savefig(os.path.join(output_dir, f"{col}.png"))
    

if __name__ == '__main__':
    visualize()