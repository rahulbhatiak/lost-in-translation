import click
import matplotlib.pyplot as plt
import os
import pandas as pd
import seaborn as sns
import unicodedata

TITLE_STRING = "Título"
DESCRIPTION_STRING = "Descripción"

HEATMAP_COLUMN_NAMES = {
   "en_title_sent": "Title EN\nSentiment",
   "en_lemma_title_sent": "Title EN\nLemmatized\nSentiment",
   "body_stem_title_sent": "Title ES\nStemmed\nSentiment" ,
   "en_desc_sent": "Desc EN\nSentiment",
   "en_lemma_desc_sent": "Desc EN\nLemmatized\nSentiment",
   "body_stem_desc_sent": "Desc ES\nStemmed\nSentiment" ,
}


def _strip_accents(text):
    try:
        text = unicode(text, 'utf-8')
    except NameError: # unicode is a default on python 3 
        pass

    text = unicodedata.normalize('NFD', text)\
           .encode('ascii', 'ignore')\
           .decode("utf-8")

    return str(text)


def build_dataset(input_filepath):
    base_file = os.path.join(input_filepath, "chequeado_spanish_processed.csv")
    translated_title_file = os.path.join(input_filepath, f"{TITLE_STRING}_full_translation.csv") 
    en_title_lemma_sent_file = os.path.join(input_filepath, f"{TITLE_STRING}_en_lemma_sentiment.csv")
    en_title_sent_file = os.path.join(input_filepath, f"{TITLE_STRING}_en_sentiment.csv")
    translated_desc_file = os.path.join(input_filepath, f"{DESCRIPTION_STRING}_full_translation.csv") 
    en_desc_lemma_sent_file = os.path.join(input_filepath, f"{DESCRIPTION_STRING}_en_lemma_sentiment.csv")
    en_desc_sent_file = os.path.join(input_filepath, f"{DESCRIPTION_STRING}_en_sentiment.csv")

    native_sent_file = os.path.join(input_filepath, "processed_text2-spanish.csv")
    
    base_df = pd.read_csv(base_file, index_col=0)
    base_df[f'{TITLE_STRING} stripped'] = base_df[TITLE_STRING].apply(_strip_accents)
    translated_title_sent = pd.read_csv(en_title_sent_file, index_col=0)
    translated_desc_sent = pd.read_csv(en_desc_sent_file, index_col=0)
    translated_title_lemma_sent = pd.read_csv(en_title_lemma_sent_file, index_col=0)
    translated_desc_lemma_sent = pd.read_csv(en_desc_lemma_sent_file, index_col=0)
    translated_title = pd.read_csv(translated_title_file, index_col=0)
    translated_desc = pd.read_csv(translated_desc_file, index_col=0)
    
    KRISTIN_COLUMNS = ['body_lem_title_sent', 'body_stem_title_sent', 'body_lem_desc_sent', 'body_stem_desc_sent']
    RAHUL_COLUMNS = ['en_lemma_title_sent', 'en_title_sent', 'en_lemma_desc_sent', 'en_desc_sent']
    native_sent = pd.read_csv(native_sent_file)
    for col in KRISTIN_COLUMNS:
        native_sent[col] = pd.to_numeric(native_sent[col], errors='coerce')
    # TODO CHECK IF THE INNER JOIN IS 1_to_1 Against native_sent
    sentiment_comparison = (
        base_df 
        .join(translated_title_sent)
        .join(translated_desc_sent)
        .join(translated_title_lemma_sent)
        .join(translated_desc_lemma_sent)
        .join(translated_title)
        .join(translated_desc)
        .join(
            native_sent.set_index('original title')[KRISTIN_COLUMNS],
            on=f'{TITLE_STRING} stripped', how='inner')
        .drop(columns=[f'{TITLE_STRING} stripped'])
        .rename(columns={
            f'{TITLE_STRING}_en_lemma_sentiment': 'en_lemma_title_sent',
            f'{DESCRIPTION_STRING}_en_lemma_sentiment': 'en_lemma_desc_sent',
            f'{TITLE_STRING}_en_sentiment': 'en_title_sent',
            f'{DESCRIPTION_STRING}_en_sentiment': 'en_desc_sent'})
    )
    # NOTE roughly 300 rows got dropped on the join on native_sent
    for col in [col for col in sentiment_comparison.columns if col.startswith('en_')]:
        sentiment_comparison[col] = (sentiment_comparison[col] + 1.0) / 2.0
    sentiment_comparison['sent_distance'] = (sentiment_comparison['body_lem_title_sent'] - sentiment_comparison['en_lemma_title_sent']).abs()
    return sentiment_comparison


@click.command("compare-sentiment")
@click.argument('input-filepath', type=click.Path())
@click.argument('output-filepath', type=click.Path())
@click.option('-f', '--force', is_flag=True, default=False)
def compare_sentiment(input_filepath, output_filepath, force):
    """Produce visualizations comparing language sentiments"""
    sent_df = build_dataset(input_filepath)
    sent_df.to_csv(os.path.join(output_filepath, "sentiment_comparison.csv"))
    sns.set()
    figure = sns.pairplot(sent_df.dropna())
    figure.savefig(os.path.join(output_filepath, "sentiment_pairplot.png"))

    for key in ["Title", "Desc"]:
        fig, ax = plt.subplots(figsize=(10, 8))
        dataset = sent_df.rename(columns=HEATMAP_COLUMN_NAMES)[[col for col in HEATMAP_COLUMN_NAMES.values() if col.startswith(key)]]
        sns.heatmap(dataset.corr(), annot=True, fmt=".1%", ax=ax)
        ax.set_xticklabels(ax.get_xticklabels(), rotation=0)
        ax.set_yticklabels(ax.get_yticklabels(), rotation=45)
        ax.set_title(f"{key} Sentiment Correlation")
        fig.tight_layout()
        fig.savefig(os.path.join(output_filepath, f"{key}_sentiment_correlation_heatmap.png"))


if __name__ == '__main__':
    compare_sentiment()
