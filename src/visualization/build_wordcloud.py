import click
import os
import pandas as pd

from wordcloud import WordCloud


@click.command("wordcloud")
@click.argument("input_file", type=click.Path())
@click.argument("output_file", type=click.Path())
@click.option('-f', '--force', is_flag=True)
def build_wordcloud(input_file, output_file, force):
    if os.path.exists(output_file) and force:
        os.remove(output_file)
    elif os.path.exists(output_file):
        print(f"{output_file} already exists. Skipping")
    else:
        column = pd.read_csv(input_file, index_col=0)
        text = " ".join(column.iloc[:, 0])
        final_text = " ".join(
            [word for word in text.split(" ") if '&quot' not in word]
        )
        wc = WordCloud().generate(final_text)
        wc.to_file(output_file)


if __name__ == '__main__':
    build_wordcloud()
