import click
import logging
import os
import pandas as pd


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def main(input_filepath, output_filepath):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    filepath = os.path.join(input_filepath, "chequeado.csv")
    chequeado_df = pd.read_csv(filepath, error_bad_lines=False)
    total_lines = len(open(filepath, 'r').readlines())
    logger.warning(f"We threw away {total_lines - chequeado_df.shape[0]} rows due to CSV errors")
    logger.info('making final data set from raw data')
    chequeado_df.to_csv(os.path.join(output_filepath, "chequeado_processed.csv"))
